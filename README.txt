INTRODUCTION
------------

Rublon provides stronger security for your online accounts through invisible 
two-factor authentication. It protects your accounts from sign-ins 
from unknown devices.

Your online accounts can be accessed from any device in the world, 
often without your knowledge. Rublon protects your personal data, 
as well as business and financial operations, by restricting access from 
unknown devices.

Use Rublon to manage and define the devices you trust, such as your laptop, 
tablet and mobile phone. Rublon-protected accounts will only be accessible from
your Trusted Devices. The Rublon mobile app allows you to add or remove 
Trusted Devices at any time, anywhere you are.

Find more about Rublon at https://www.rublon.com.

HOW DOES IT WORK
----------------

Rublon is an additional security layer that verifies whether you are signing in
from a Trusted Device. It works on top of any existing authentication process.
This means that you still have to type in your username and password or use
a social login button to sign in, but it must be done on a Trusted Device 
in order to access your account. And if you want to sign in using a new device,
simply confirm your identity using Rublon, and then add it to your Trusted
Devices.

You can secure your data by protecting your account with Rublon. Go to the 
"Your Profile -> Edit" section and click "Protect your account". If you don't 
have a Trusted Device, you will be prompted to scan a QR code with the 
Rublon mobile app. After scanning the QR code you will decide if you 
want to make your current browser a Trusted Device. 

F.A.Q.
------

Q: How do I use Rublon?
A: In order to use Rublon in your Drupal installation please go to 
"Your Profile -> Edit" section and click "Protect your account".

Q: Is Rublon available for my smartphone?
A: Currently Rublon is available for users of smartphones with the Android
system (e.g. HTC, LG, Motorola, Samsung, Sony), the iPhone/iPod, smartphones 
with Windows Phone system (e.g. HTC, Nokia Lumia) and the BlackBerry.
Go get it: http://rublon.com/get.

Q: How do I install the Rublon mobile app?
A: Go to http://rublon.com, click the big green button ("Free download") 
and follow the instructions.

Q: How much does it cost to use Rublon?
A: Rublon is available free of charge.

Q: Do I have to use my smartphone at each sign in?
A: No! Your smartphone is only needed when you sign in from a new device. 
It is used to prove your identity and allows you to add such a device to your 
trusted devices. Any sign in from a trusted device works the same way as 
before: simply enter your login credentials. 
Rublon works as an invisible security layer during the sign in process.
