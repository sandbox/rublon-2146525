<?php

/**
 * @file
 * Rublon.text class file.
 */

/**
 * Helper class for l18n purposes.
 */
class RublonText {

  /**
   * Get a localized message.
   *
   * @param string $code
   *   Message code.
   */
  public static function getMessage($code) {
    $t = get_t();
    switch ($code) {
      case 'RC_UPDATE_AVAILABLE':
        $result = $t("Rublon module update is already available.");
        break;

      case 'RC_UPDATE_AVAILABLE_TEXT':
        $result = $t("To provide the highest security level for your Drupal we recommend to install the recent extension version.");
        break;

      case 'RC_UPDATE_AVAILABLE_DOWNLOAD_LINK':
        $result = $t("Download Update");
        break;

      case 'CR_PLUGIN_OUTDATED':
        $result = $t("This version of the Rublon extension is already outdated. Before you activate Rublon please install the latest version.");
        break;

      case 'RC_FAILED_TO_AUTHENTICATE_USER':
        $result = $t("Failed to authenticate user. Please try again.");
        break;

      case 'RC_UNKNOWN_ACTION_TO_PERFORM':
        $result = $t("Unknown action to perform.");
        break;

      case 'RC_NOT_AUTHENTICATED_FIRST_FACTOR':
        $result = $t("User is not authenticated by first factor (login and password).");
        break;

      case 'RC_EXPECTED_DIFFERENT_USER':
        $result = $t("Expected different user.");
        break;

      case 'RC_DRUPAL_AUTH_PROBLEM':
        $result = $t("An error has occurred when trying to authenticate user in Drupal.");
        break;

      case 'RC_RUBLON_ENABLED':
        $result = $t("Your account is now protected by Rublon.");
        break;

      case 'RC_RUBLON_DISABLED':
        $result = $t("Rublon protection has been disabled. You are now protected by a password only, which may result in unauthorized access to your account. We strongly encourage you to protect your account with Rublon.");
        break;

      case 'RC_ERROR':
        $result = $t("Rublon server error. Please try again in a moment.");
        break;

      case 'CR_SUCCESS':
        $result = $t("Thank you! Now all of your users can protect their accounts with Rublon.");
        break;

      case 'CR_ERROR':
        $result = $t("Rublon activation failed. Please try again. Should the error occur again, contact us at @support_url.", array("@support_url" => "<a href='mailto:support@rublon.com'>support@rublon.com</a>"));
        break;

      case 'IN_NOTIFICATION_SENT':
        $result = $t("Issue notification has been sent to the Rublon support team.");
        break;

      case 'RD_ICON_PROTECTED':
        $result = $t("Your account is protected by Rublon");
        break;

      case 'RD_ERROR':
        $result = $t("An error has occurred. Please try again.");
        break;

      case 'RD_ICON_TITLE':
        $result = $t("Rublon Two-Factor Authentication");
        break;

      case 'RD_SDK_NOT_LOADED':
        $result = $t("The Rublon PHP SDK library could not be found. Please check the installation instructions and the <a href=\"@status\">Status Report</a>.",
                  array('@status' => url('admin/reports/status')));
        break;

      case 'RD_SDK_REQUIRED':
        $library = libraries_detect('rublon-php-sdk');
        $result = $t("The Rublon PHP SDK library could not be found. Download it from <a href=\"@rublonphpsdk\" target=\"_blank\">developers.rublon.com</a>.",
          array('@rublonphpsdk' => $library['download url']));
        break;

      case 'RD_SDK_LIBRARY':
        $result = $t("Rublon PHP SDK library");
        break;

      case 'RD_SDK_OLD_VERSION':
        $result = $t("This Rublon PHP SDK library version is outdated. Please update to Rublon PHP SDK version @rublonsdkversion.",
          array('@rublonsdkversion' => RublonDrupal::RUBLON_PHP_SDK_VERSION_REQUIRED));
        break;

      case 'RD_CURL_LIBRARY':
        $result = $t("cURL library");
        break;

      case 'RD_CURL_AVAILABLE':
        $result = $t("cURL library enabled");
        break;

      case 'RD_CURL_NOT_AVAILABLE':
        $result = $t("cURL library not found");
        break;

      case 'RD_CURL_ERROR':
        $result = $t("Rublon requires the PHP <a href='@curl_url' target=\"_blank\">cURL</a> library.",
          array('@curl_url' => 'http://php.net/manual/en/curl.setup.php'));
        break;

      default:
        $result = $t("An error has occurred. Please try again.");

    }
    return $result;
  }

}
