<?php

/**
 * @file
 * Rublon.drupal class file.
 */

module_load_include('inc', 'rublon', 'includes/rublon.issuenotifier');
module_load_include('inc', 'rublon', 'includes/rublon.drupal.issuenotifier');
module_load_include('inc', 'rublon', 'includes/rublon.text');


/**
 * Rublon for Drupal module's helper class.
 */
class RublonDrupal {

  /**
   * Plugin's version.
   *
   * @var string
   */
  const VERSION = '1.0.0';

  /**
   * Plugin's version date.
   *
   * @var string
   */
  const VERSION_DATE = '2014-04-17';

  /**
   * The required Rublon PHP SDK library version.
   *
   * @var string
   */
  const RUBLON_PHP_SDK_VERSION_REQUIRED = '2013-11-22';

  /**
   * Table name for local users and Rublon profile ID association.
   *
   * @var string
   */
  const TABLE_NAME = 'rublon2factor';

  /**
   * Rublon table's field name with Rublon profile ID.
   *
   * @var string
   */
  const TABLE_FIELD_PROFILE_ID = 'rublon_profile_id';

  /**
   * Rublon table's field name with user's ID.
   *
   * @var string
   */
  const TABLE_FIELD_USER_ID = 'user_id';

  /**
   * Drupal variable name to store system token.
   *
   * @var string
   */
  const VARIABLE_SYSTEM_TOKEN = 'rublon_system_token';

  /**
   * Drupal variable name to store secret key.
   *
   * @var string
   */
  const VARIABLE_SECRET_KEY = 'rublon_secret_key';

  /**
   * Name of the administrator role.
   *
   * @var string
   */
  const ROLE_ADMIN = 'administrator';

  /**
   * Role name for authenticated user.
   *
   * @var string
   */
  const ROLE_AUTHENTICATED_USER = 'authenticated user';

  /**
   * Rublon configuration URL internal path.
   *
   * @var string
   */
  const URL_CONFIG = 'admin/config/rublon';

  /**
   * Activation URL internal path.
   *
   * @var string
   */
  const URL_CONFIG_ACTIVATION = 'admin/config/rublon/activation';

  /**
   * Callback URL internal path.
   *
   * @var string
   */
  const URL_CALLBACK = 'rublon/callback';

  /**
   * URL to preloaded image sprite.
   */
  const URL_IMAGE_SPRITE = "/public/img/sprite.png";

  /**
   * URL to checking updates on Rublon Developers.
   */
  const API_URL_UPDATES = '/updates/drupal';

  /**
   * Technology code name.
   *
   * @var string
   */
  const TECHNOLOGY = 'drupal';

  /**
   * Protected icon title.
   *
   * @var string
   */
  const TEXT_ICON_PROTECTED = "Your account is protected by Rublon";

  /**
   * Template for HTML icon "Rublon Seal".
   */
  const TEMPLATE_ICON_SEAL = '<a href="https://rublon.com" title="%s" target="_blank" style="display:block;width:79px;height:30px;background: url(%s) 0px -336px no-repeat;float:right;"></a><div style="clear:right"></div>';

  /**
   * Template for HTML protected icon.
   */
  const TEMPLATE_ICON_PROTECTED = '<div style="width:16px;height:16px;background:url(%s) -219px -378px no-repeat;display:inline-block;vertical-align:middle;margin-left:0.5em;" title="%s" class="rublon-protected-icon"></div>';

  /**
   * Template for the update message.
   */
  const TEMPLATE_UPDATE_MESSAGE = '<strong>%s</strong><div>%s</div><div class="rublon-download-update"><a href="%s" target="_blank">%s</a></div>';

  /**
   * Session key for extension update info.
   */
  const SESSION_UPDATE_INFO = 'rublon_update_info';


  /**
   * Instance of the singleton.
   *
   * @var RublonDrupal
   */
  protected static $instance;

  /**
   * Callback instance.
   *
   * @var RublonCallback
   */
  protected $callback = NULL;

  /**
   * Instance of the issue notifier.
   *
   * @var RublonDrupalIssueNotifier
   */
  protected $issueNotifier = NULL;


  /**
   * Singleton only - use static get() method.
   */
  protected function __construct() {
    registry_rebuild();
    libraries_load('rublon-php-sdk');
    $this->issueNotifier = new RublonDrupalIssueNotifier($this);
  }


  /**
   * Get singleton instance.
   */
  public static function get() {
    if (empty(self::$instance)) {
      self::$instance = new RublonDrupal();
    }
    return self::$instance;
  }

  /**
   * Add error message.
   *
   * @param mixed $error
   *   The error parameter, may be mixed type.
   * @param array $notifier_options
   *   The notifierOptions parameter, must be type array.
   */
  public static function error($error, array $notifier_options = array()) {

    // Prepare error message.
    if (is_object($error) AND $error instanceof Exception) {
      $message = $error->getMessage();
    }
    else {
      $message = RublonText::getMessage((string) $error);
      if (!strlen($message)) {
        $message = RublonText::getMessage('RD_ERROR');
      }
    }

    // Check if the message to be added hasn't been queued
    // For displaying earlier - if so, do not queue it again.
    $add_message = TRUE;
    $drupal_messages = drupal_get_messages();
    if (!empty($drupal_messages['error'])) {
      if (in_array($message, $drupal_messages['error'])) {
        $add_message = FALSE;
      }
    }

    // Add message to the Drupal message queue.
    if ($add_message) {
      // Add flash message:
      drupal_set_message($message, 'error');

      // Send notify:
      if (!empty($notifier_options)) {
        self::get()->notify($error, $notifier_options);
      }
    }

  }


  /**
   * Notify Rublon team about issue.
   *
   * @param mixed $error
   *   The error parameter, may be mixed type.
   * @param string $notifier_options
   *   The notifierOptions parameter, must be type string.
   */
  public function notify($error, $notifier_options) {
    $this->issueNotifier->notify($error, $notifier_options);
  }

  /**
   * Get system token.
   */
  public static function getSystemToken() {
    return variable_get(self::VARIABLE_SYSTEM_TOKEN);
  }

  /**
   * Save system token.
   *
   * @param string $system_token
   *   The systemToken parameter, must be type string.
   */
  public static function saveSystemToken($system_token) {
    variable_set(self::VARIABLE_SYSTEM_TOKEN, $system_token);
    return TRUE;
  }


  /**
   * Get secret key.
   */
  public static function getSecretKey() {
    return variable_get(self::VARIABLE_SECRET_KEY);
  }

  /**
   * Save secret key.
   *
   * @param string $secret_key
   *   The secretKey parameter, must be type string.
   */
  public static function saveSecretKey($secret_key) {
    variable_set(self::VARIABLE_SECRET_KEY, $secret_key);
    return TRUE;
  }

  /**
   * Get language code.
   */
  public static function getLang() {
    global $language;
    return $language->language;
  }

  /**
   * Get API domain.
   */
  public static function getAPIDomain() {
    return RublonConsumer::DEFAULT_API_DOMAIN;
  }


  /**
   * Get URL to the currnet user edit page.
   */
  public static function getUserEditURL() {
    global $user;
    if (!empty($user) AND !empty($user->uid)) {
      return url('user/' . $user->uid . '/edit', array('absolute' => TRUE));
    }
    else {
      return url(NULL, array('absolute' => TRUE));
    }
  }

  /**
   * Check if all configuration variables are set.
   */
  public static function isConfigured() {
    return (self::getSystemToken()
      AND self::getSecretKey());
  }

  /**
   * Check whether current user can activate Rublon module.
   */
  public static function canActivate() {
    global $user;
    return (self::isUserAuthenticated()
      AND in_array(self::ROLE_ADMIN, $user->roles));
  }

  /**
   * Get Rublon profile ID of user with given UID.
   *
   * @param int $uid
   *   The uid parameter, must be type int.
   */
  public static function getRublonProfileId($uid = NULL) {
    global $user;
    if (empty($uid)) {
      if (!empty($user) AND is_object($user) AND !empty($user->uid)) {
        $uid = $user->uid;
      }
      else {
        return NULL;
      }
    }
    $user = user_load($uid);
    if (!empty($user)) {

      $result = db_select(self::TABLE_NAME, 'r2f')
        ->fields('r2f', array(self::TABLE_FIELD_PROFILE_ID))
        ->where(self::TABLE_FIELD_USER_ID . ' = :uid', array(':uid' => $user->uid))
        ->range(0, 1)
        ->execute();
      if ($result) {
        return $result->fetchField();
      }

    }
  }


  /**
   * Check whether current user is authenticated.
   *
   * @param string $uid
   *   (optional) Check if current user has given UID.
   */
  public static function isUserAuthenticated($uid = NULL) {
    global $user;
    return (!empty($user)
      AND is_object($user)
      AND !empty($user->uid)
      AND in_array(self::ROLE_AUTHENTICATED_USER, $user->roles)
      AND (empty($uid) OR $uid == $user->uid)
    );
  }


  /**
   * Set Rublon profile ID for current user.
   *
   * @param int $profile_id
   *   New Rublon profile ID.
   */
  public static function setRublonProfileId($profile_id) {
    global $user;
    if (!empty($user) AND is_object($user) AND !empty($user->uid)) {
      // Load user bypassing cache:
      $user = user_load($user->uid);

      $result = db_select(self::TABLE_NAME, 'r2f')
        ->fields('r2f')
        ->where(self::TABLE_FIELD_USER_ID . ' = :uid', array(':uid' => intval($user->uid)))
        ->execute();

      if ($result AND $result->rowCount() > 0) {
        // Update user's profile ID:
        db_update(self::TABLE_NAME)
          ->fields(array(
            self::TABLE_FIELD_PROFILE_ID => intval($profile_id),
          ))
          ->where(self::TABLE_FIELD_USER_ID . ' = :uid', array(':uid' => intval($user->uid)))
          ->execute();
      }
      else {
        // Insert user's profile ID:
        db_insert(self::TABLE_NAME)
          ->fields(array(
            self::TABLE_FIELD_USER_ID => intval($user->uid),
            self::TABLE_FIELD_PROFILE_ID => intval($profile_id),
          ))
          ->execute();
      }

      return TRUE;
    }
    return FALSE;
  }


  /**
   * Returns activation absolute URL.
   *
   * @param string $action
   *   (optional) Create an URL with given action.
   */
  public static function getActivationURL($action = NULL) {
    $params = array('absolute' => TRUE);
    if (!empty($action)) {
      $params = array('query' => array('action' => $action));
    }
    return url(self::URL_CONFIG_ACTIVATION, $params);
  }


  /**
   * Returns callback absolute URL.
   */
  public static function getCallbackURL() {
    return url(self::URL_CALLBACK, array('absolute' => TRUE));
  }


  /**
   * Reset configuration (eg. for tests).
   */
  public static function resetConfiguration() {
    self::setRublonProfileId(0);
    variable_del(self::VARIABLE_SYSTEM_TOKEN);
    variable_del(self::VARIABLE_SECRET_KEY);
  }


  /**
   * Redirect to given URL.
   *
   * @param string $url
   *   The url parameter, must be type string.
   * @param string $internal
   *   (optional) Internal URL, default TRUE.
   */
  public static function redirect($url, $internal = TRUE) {
    if ($internal) {
      drupal_goto($url);
    }
    else {
      header('Location: ' . $url);
      exit;
    }
  }

  /**
   * Returns the GET parameter.
   *
   * @param string $name
   *   The name parameter, must be type string.
   */
  public static function inputGet($name) {
    $query = drupal_get_query_parameters();
    return (isset($query[$name]) ? $query[$name] : NULL);
  }

  /**
   * Get technology code name.
   */
  public static function getTechnology() {
    return self::TECHNOLOGY;
  }


  /**
   * Login user with given uid.
   *
   * @param int $uid
   *   The uid parameter, must be type int.
   */
  public static function loginUser($uid) {
    global $user;
    $user = user_load($uid);
    user_login_finalize();
  }


  /**
   * Prevent browser caching.
   */
  public static function disableCaching() {
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0', FALSE);
    header('Pragma: no-cache');
  }


  /**
   * Get HTML code with Rublon seal icon for login form.
   *
   * @return string
   *   HTML code to be injected into login form
   */
  public static function getIconSeal() {
    return sprintf(self::TEMPLATE_ICON_SEAL,
      htmlspecialchars(RublonText::getMessage('RD_ICON_TITLE')),
      htmlspecialchars(self::getAPIDomain() . self::URL_IMAGE_SPRITE)
    );
  }


  /**
   * Get HTML code with Rublon icon for welcome message.
   *
   * @return string
   *   HTML code to be injected into the toolbar
   */
  public static function getIconProtected() {
    return sprintf(self::TEMPLATE_ICON_PROTECTED,
      self::getAPIDomain() . self::URL_IMAGE_SPRITE,
      htmlspecialchars(RublonText::getMessage('RD_ICON_PROTECTED'))
    );
  }


  /**
   * Hook toolbar.
   *
   * @param array $toolbar
   *   The toolbar parameter, must be type array.
   *
   * @return array
   *   The modified toolbar
   */
  public static function hookToolbar(array $toolbar) {
    // Add protected icon.
    $toolbar['toolbar_user']['#links']['account']['title'] .= ' '
      . RublonDrupal::getIconProtected();
    return $toolbar;
  }


  /**
   * Get URL of the Rublon icon.
   *
   * @param int $size
   *   The size parameter, must be type int.
   *
   * @return string
   *   URL address of the icon
   */
  public static function getRublonIconURL($size) {
    return RublonConsumer::DEFAULT_API_DOMAIN
      . '/public/img/icons/R_' . $size . 'x' . $size . '.png';
  }


  /**
   * Get base URL.
   *
   * @return string
   *   Website's base URL
   */
  public static function getProjectURL() {
    return url(NULL, array('absolute' => TRUE));
  }


  /**
   * Initialize data model (create database table).
   *
   * @return array
   *   The modified database schema array
   */
  public static function initializeDataModel() {
    $schema = array();
    $schema[RublonDrupal::TABLE_NAME] = array(
      'description' => 'Store Rublon-specific data',
      'fields' => array(
        RublonDrupal::TABLE_FIELD_USER_ID => array(
          'description' => 'A user\'s ID in the Drupal database.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ),
        RublonDrupal::TABLE_FIELD_PROFILE_ID => array(
          'description' => 'A user\'s Rublon Profile ID.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array(RublonDrupal::TABLE_FIELD_USER_ID),
    );
    return $schema;
    /* db_query('CREATE TABLE IF NOT EXISTS `'. RublonDrupal::TABLE_NAME .'` (
    `'. RublonDrupal::TABLE_FIELD_USER_ID .'` int(10) unsigned NOT NULL,
    `'. RublonDrupal::TABLE_FIELD_PROFILE_ID .'` int(10) unsigned NOT NULL,
    PRIMARY KEY (`'. RublonDrupal::TABLE_FIELD_USER_ID .'`)
    ) ENGINE=MyISAM  DEFAULT CHARSET=utf8;'); */
  }

  // ---------------------------------------------------------------------------
  // HOOKS and PAGES:
  /**
   * Register an URL routes.
   *
   * Implements hook_menu().
   *
   * @return array
   *   Modified routes array
   * @see rublon_menu()
   */
  public function hookMenu() {
    $items = array();
    $items['admin/config/rublon'] = array(
      'title' => 'Rublon config',
      'description' => 'Configuration for Rublon module',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('_rublon_activation_page'),
      'access arguments' => array('access administration pages'),
      // A hidden, internal callback, typically used for API calls.
      'type' => MENU_CALLBACK,
    );
    $items['rublon'] = array(
      'title' => 'Rublon page',
      'page callback' => '_rublon_callback',
      'access callback' => TRUE,
      // A hidden, internal callback, typically used for API calls.
      'type' => MENU_CALLBACK,
    );
    return $items;
  }


  /**
   * Run Rublon protection after user login.
   *
   * Implements hook_user_login().
   *
   * @param array $edit
   *   The edit parameter, must be type array.
   * @param object $account
   *   The account parameter, must be type object.
   *
   * @see rublon_user_login()
   */
  public function hookUserLogin(array &$edit, $account) {

    // Rublon is configured, user's account is protected by Rublon and callback
    // Isn't in progress.
    if (self::isConfigured() AND $profile_id = self::getRublonProfileId() AND empty($this->callback)) {

      session_destroy();
      drupal_session_destroy_uid($account->uid);

      $service = $this->getRublonService();
      $consumer_params = array(
        'uid' => $account->uid,
        'session_id' => session_id(),
        'session_name' => session_name(),
      );
      $destination = array();
      if ($destination = drupal_get_destination() AND !empty($destination['destination'])) {
        $consumer_params['customURIParam'] = $destination['destination'];
      }
      else {
        $consumer_params['customURIParam'] = current_path();
      }
      $service->initAuthorization($profile_id, $consumer_params);
      exit;
    }

  }


  /**
   * Rublon module's activation page.
   *
   * @see _rublon_activation_page()
   */
  public function pageActivation() {
    self::disableCaching();
    $query = drupal_get_query_parameters();
    if (isset($query['action'])) {
      module_load_include('inc', 'rublon', 'includes/rublon.consumer.registration.template');
      module_load_include('inc', 'rublon', 'includes/rublon.consumer.registration');
      $registration = new RublonConsumerRegistration();
      $registration->action($query['action']);
    }
    else {
      self::redirect(self::isUserAuthenticated() ? self::getUserEditURL() : NULL);
    }
  }


  /**
   * Implements hook_form_FORM_ID_alter() for user_profile_form().
   *
   * Remove the profile ID input field and append Rublon GUI.
   *
   * @see rublon_form_user_profile_form_alter()
   */
  public function hookUserEditForm(&$form, &$form_state) {
    global $user;

    // Append Rublon GUI.
    if ($form['#user_category'] == 'account') {

      $account = $form['#user'];
      if ($user AND $account->uid == $user->uid) {

        self::hookUpdateMessage();

        $form['rublon'] = array(
          '#type' => 'fieldset',
          '#title' => 'Rublon',
          '#weight' => 1,
          '#collapsible' => FALSE,
          '#tree' => TRUE,
        );

        $form['rublon']['gui'] = array(
          '#markup' => $this->getUserBox(),
        );

      }
    }
  }


  /**
   * Append Rublon Seal icon to the login form.
   *
   * Implements hook_form_FORM_ID_alter().
   *
   * @param array $form
   *   The form parameter, must be type array.
   * @param array $form_state
   *   The form_state parameter, must be type array.
   *
   * @see rublon_form_user_login_block_alter()
   */
  public function hookLoginForm(array &$form, array &$form_state) {
    if (self::isConfigured()) {
      $content = self::getIconSeal();
      $form['#attached']['css'] = array(
        drupal_get_path('module', 'rublon') . '/assets/css/rublon.css',
      );
      $form['actions'][] = array(
        '#prefix' => '<div class="rublon-seal">',
        '#markup' => $content,
        '#suffix' => '</div>',
      );
    }
  }


  /**
   * Run Rublon callback.
   *
   * @see _rublon_callback()
   */
  public function callback() {
    self::disableCaching();
    module_load_include('inc', 'rublon', 'includes/rublon.callback');
    try {
      $this->callback = new RublonCallback();
      $this->callback->run();
    }
    catch (Exception $e) {
      self::error('RC_FAILED_TO_AUTHENTICATE_USER', array(
        'method' => __METHOD__,
        'file' => __FILE__,
        'exception' => $e,
      ));
      $this->callback->goBack();
    }
    exit;
  }


  // ---------------------------------------------------------------------------
  // PROTECTED:
  /**
   * Construct and return Rublon service instance.
   *
   * @return RublonService2Factor
   *   Rublon service instance
   */
  protected function getRublonService() {
    $consumer = new RublonConsumer(self::getSystemToken(), self::getSecretKey());
    $consumer->setDomain(self::getAPIDomain());
    $consumer->setLang(self::getLang());
    $consumer->setTechnology(self::getTechnology());
    $service = new RublonService2Factor($consumer);
    return $service;
  }


  /**
   * Get user box.
   *
   * @return string
   *   HTML/JS code to be injected into the user's profile page
   */
  protected function getUserBox() {
    module_load_include('inc', 'rublon', 'includes/rublon.gui');
    $gui = new RublonGUI();
    return $gui->userBox();
  }


  /**
   * Check whether module is up-to-date and show message.
   */
  public static function hookUpdateMessage() {
    if (self::canActivate() AND !self::isUpToDate(FALSE)) {
      self::showUpdateMessage();
    }
  }


  /**
   * Show Rublon update message.
   */
  protected static function showUpdateMessage() {
    if ($update = self::getVersionRecord()) {

      $message = sprintf(self::TEMPLATE_UPDATE_MESSAGE,
        RublonText::getMessage('RC_UPDATE_AVAILABLE'),
        RublonText::getMessage('RC_UPDATE_AVAILABLE_TEXT'),
        $update['external_url'],
        RublonText::getMessage('RC_UPDATE_AVAILABLE_DOWNLOAD_LINK')
      );

      // Check if the message to be added hasn't been queued
      // For displaying earlier - if so, do not queue it again.
      $add_message = TRUE;
      $drupal_messages = drupal_get_messages(NULL, FALSE);
      if (!empty($drupal_messages['warning'])) {
        if (in_array($message, $drupal_messages['warning'])) {
          $add_message = FALSE;
        }
      }

      if ($add_message) {
        drupal_set_message($message, 'warning');
      }
    }
  }


  /**
   * Get value from session.
   *
   * @param string $name
   *   The name parameter, must be type string.
   *
   * @return mixed
   *   Value of a given session key
   */
  public static function sessionGet($name) {
    if (isset($_SESSION[$name])) {
      return $_SESSION[$name];
    }
  }

  /**
   * Set value to the session.
   *
   * @param string $name
   *   The name parameter, must be type string.
   * @param mixed $value
   *   The value parameter, may be mixed type.
   */
  public static function sessionSet($name, $value) {
    $_SESSION[$name] = $value;
  }


  /**
   * Check if extension is up-to-date.
   *
   * @param bool $cache
   *   Get cached value from session (default true).
   *
   * @return bool
   *   True if the latest version is installed
   */
  public static function isUpToDate($cache = TRUE) {
    return version_compare(self::VERSION, self::getLatestVersion($cache), '>=');
  }


  /**
   * Get the latest version of the extension available on Rublon Developers.
   *
   * @param bool $cache
   *   Get cached value from session (default true).
   *
   * @return string|null
   *   The latest module version
   */
  public static function getLatestVersion($cache = TRUE) {
    $response = self::getVersionRecord($cache);
    if (!empty($response) AND !empty($response['version'])) {
      return $response['version'];
    }
  }


  /**
   * Send request to check updates.
   *
   * @return array
   *   Rublon server's response
   */
  public static function getVersionRecord($cache = TRUE) {
    $response = self::sessionGet(self::SESSION_UPDATE_INFO);
    if (!$cache OR empty($response)) {
      module_load_include('inc', 'rublon', 'includes/rublon.consumer.registration.template');
      module_load_include('inc', 'rublon', 'includes/rublon.consumer.registration');
      $registration = new RublonConsumerRegistration();
      $url = $registration->getAPIDomain() . self::API_URL_UPDATES;
      $context = stream_context_create(array('http' => array('timeout' => 5)));
      $response = json_decode(file_get_contents($url, FALSE, $context), TRUE);
      self::sessionSet(self::SESSION_UPDATE_INFO, $response);
    }
    return $response;
  }


}
