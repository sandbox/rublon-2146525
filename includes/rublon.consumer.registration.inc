<?php

/**
 * @file
 * Rublon.consumer.registration class file.
 */

/**
 * Implementation of the RublonConsumerRegistrationTemplate.
 *
 * @see RublonConsumerRegistrationTemplate
 */
class RublonConsumerRegistration extends RublonConsumerRegistrationTemplate {

  /**
   * Drupal variable name to store temp key.
   *
   * @var string
   */
  const VARIABLE_TEMP_KEY = 'rublon_activation_temp_key';

  /**
   * Drupal variable name to store start time.
   *
   * @var string
   */
  const VARIABLE_START_TIME = 'rublon_activation_start_time';

  /**
   * Final method when process was successful.
   *
   * @see RublonConsumerRegistrationTemplate::finalSuccess()
   */
  protected function finalSuccess() {
    parent::finalSuccess();
    drupal_set_message(RublonText::getMessage('CR_SUCCESS'));
    RublonDrupal::redirect(RublonDrupal::getUserEditURL());
  }


  /**
   * Final method when process was failed.
   *
   * @see RublonConsumerRegistrationTemplate::finalError()
   */
  protected function finalError($msg = NULL) {
    parent::finalError($msg);

    if (empty($msg)) {
      $url_params = drupal_get_query_parameters();
      if (array_key_exists('error', $url_params)) {
        $msg = $url_params['error'];
      }
    }

    $error_code = '';
    if (!empty($msg)) {
      if (stripos($msg, 'ERROR_CODE:') !== FALSE) {
        $error_code = str_replace('ERROR_CODE: ', '', $msg);
      }
    }

    RublonDrupal::error('CR_' . $error_code, array(
      'method' => __METHOD__,
      'file' => __FILE__,
      'error' => 'CR_' . $error_code,
    ));

    // Redirect:
    if (RublonDrupal::isUserAuthenticated()) {
      RublonDrupal::redirect(RublonDrupal::getUserEditURL());
    }
    else {
      RublonDrupal::redirect(NULL);
    }
    exit;

  }

  /**
   * Check user's authorization status.
   *
   * Check if current user is authorized to perform registration of the Rublon
   * module.
   *
   * @see RublonConsumerRegistrationTemplate::isUserAuthorized()
   */
  protected function isUserAuthorized() {
    return RublonDrupal::canActivate();
  }

  /**
   * Get current system token from local storage.
   *
   * @see RublonConsumerRegistrationTemplate::getSystemToken()
   */
  protected function getSystemToken() {
    return RublonDrupal::getSystemToken();
  }

  /**
   * Save system token to the local storage.
   *
   * @see RublonConsumerRegistrationTemplate::saveSystemToken()
   */
  protected function saveSystemToken($system_token) {
    return RublonDrupal::saveSystemToken($system_token);
  }

  /**
   * Get current secret key from local storage.
   *
   * @see RublonConsumerRegistrationTemplate::getSecretKey()
   */
  protected function getSecretKey() {
    return RublonDrupal::getSecretKey();
  }

  /**
   * Save secret key to the local storage.
   *
   * @see RublonConsumerRegistrationTemplate::saveSecretKey()
   */
  protected function saveSecretKey($secret_key) {
    return RublonDrupal::saveSecretKey($secret_key);
  }

  /**
   * Handle profileId of the user registering the consumer.
   *
   * @see RublonConsumerRegistrationTemplate::handleProfileId()
   */
  protected function handleProfileId($profile_id) {
    return RublonDrupal::setRublonProfileId($profile_id);
  }

  /**
   * Get temporary key from local storage.
   *
   * @see RublonConsumerRegistrationTemplate::getTempKey()
   */
  protected function getTempKey() {
    return variable_get(self::VARIABLE_TEMP_KEY);
  }

  /**
   * Save temporary key to the local storage.
   *
   * @see RublonConsumerRegistrationTemplate::saveTempKey()
   */
  protected function saveTempKey($temp_key) {
    variable_set(self::VARIABLE_TEMP_KEY, $temp_key);
    return TRUE;
  }

  /**
   * Save initial parameters to the local storage.
   *
   * @see RublonConsumerRegistrationTemplate::saveInitialParameters()
   */
  protected function saveInitialParameters($temp_key, $start_time) {
    variable_set(self::VARIABLE_START_TIME, $start_time);
    $this->saveTempKey($temp_key);
    return TRUE;
  }

  /**
   * Get process start time from local storage.
   *
   * @see RublonConsumerRegistrationTemplate::getStartTime()
   */
  protected function getStartTime() {
    return variable_get(self::VARIABLE_START_TIME);
  }

  /**
   * Get the communication URL of this Rublon module.
   *
   * @see RublonConsumerRegistrationTemplate::getCommunicationUrl()
   */
  protected function getCommunicationUrl() {
    return RublonDrupal::getActivationURL();
  }

  /**
   * Get project's public webroot URL address.
   *
   * @see RublonConsumerRegistrationTemplate::getProjectUrl()
   */
  protected function getProjectUrl() {
    return RublonDrupal::getProjectURL();
  }

  /**
   * Get the callback URL of this Rublon module.
   *
   * @see RublonConsumerRegistrationTemplate::getCallbackUrl()
   */
  protected function getCallbackUrl() {
    return RublonDrupal::getCallbackURL();
  }


  /**
   * Get project's technology.
   *
   * @see RublonConsumerRegistrationTemplate::setDomain()
   */
  public function setDomain($domain) {
    $this->apiDomain = $domain;
  }


  /**
   * Get name of the project.
   *
   * @see RublonConsumerRegistrationTemplate::getProjectName()
   */
  protected function getProjectName() {
    return variable_get('site_name');
  }


  /**
   * Get project's technology.
   *
   * @see RublonConsumerRegistrationTemplate::getProjectTechnology()
   */
  protected function getProjectTechnology() {
    return RublonDrupal::TECHNOLOGY;
  }


}
