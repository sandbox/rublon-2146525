<?php

/**
 * @file
 * Rublon.gui class file.
 */

module_load_include('inc', 'rublon', 'includes/rublon.consumer.registration.template');
module_load_include('inc', 'rublon', 'includes/rublon.consumer.registration');

/**
 * Implementation of the Rublon2FactorGUITemplate.
 *
 * @see Rublon2FactorGUITemplate
 */
class RublonGUI extends Rublon2FactorGUITemplate {


  /**
   * Get session ID or NULL if not set.
   *
   * @see Rublon2FactorGUITemplate::getSessionId()
   */
  protected function getSessionId() {
    return session_id();
  }

  /**
   * Get Rublon profile ID of the user in current session or 0 if not set.
   *
   * @see Rublon2FactorGUITemplate::getRublonProfileId()
   */
  protected function getRublonProfileId() {
    return RublonDrupal::getRublonProfileId();
  }

  /**
   * Retrieve consumer's systemToken or NULL if not set.
   *
   * @see Rublon2FactorGUITemplate::getSystemToken()
   */
  protected function getSystemToken() {
    return RublonDrupal::getSystemToken();
  }

  /**
   * Retrieve consumer's secretKey or NULL if not set.
   *
   * @see Rublon2FactorGUITemplate::getSecretKey()
   */
  protected function getSecretKey() {
    return RublonDrupal::getSecretKey();
  }

  /**
   * Return the (optional) Rublon API domain.
   *
   * @see Rublon2FactorGUITemplate::getAPIDomain()
   */
  protected function getAPIDomain() {
    return RublonDrupal::getAPIDomain();
  }

  /**
   * Check plugin activation status.
   *
   * Check whether plugin's activation is provided and user can activate the
   * plugin.
   *
   * @see Rublon2FactorGUITemplate::canActivate()
   */
  protected function canActivate() {
    return RublonDrupal::canActivate();
  }

  /**
   * Check plugin configuration status.
   *
   * Check whether Rublon plugin is configured (the system tokend and secret
   * key are defined).
   *
   * @see Rublon2FactorGUITemplate::isConfigured()
   */
  protected function isConfigured() {
    return RublonDrupal::isConfigured();
  }

  /**
   * Get URL of the activation process.
   *
   * @see Rublon2FactorGUITemplate::getActivationURL()
   */
  protected function getActivationURL() {
    return RublonDrupal::getActivationURL(RublonConsumerRegistration::ACTION_INITIALIZE);
  }

  /**
   * Returns consumer parameters for auth transaction.
   *
   * @see Rublon2FactorGUITemplate::getConsumerParams()
   */
  protected function getConsumerParams() {
    global $user;
    return array(
      'uid' => ((!empty($user) AND !empty($user->uid)) ? $user->uid : NULL),
      'sessionName' => session_name(),
      'customURIParam' => current_path(),
    );
  }

  /**
   * Get current language code.
   *
   * @see Rublon2FactorGUITemplate::getLang()
   */
  protected function getLang() {
    return RublonDrupal::getLang();
  }

  /**
   * Get technology code name.
   *
   * @see Rublon2FactorGUITemplate::getTechnology()
   */
  protected function getTechnology() {
    return RublonDrupal::getTechnology();
  }


}
