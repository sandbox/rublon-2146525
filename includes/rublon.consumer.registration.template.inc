<?php

/**
 * @file
 * Rublon.consumer.registration.template class file.
 */

/**
 * Class performing a quick registration of the Rublon module.
 *
 * Template method pattern abstract class that provides
 * main methods for communication with module registration API.
 */
abstract class RublonConsumerRegistrationTemplate {

  /**
   * Library version date.
   *
   * @var string
   */
  const VERSION = '2013-11-22';

  /**
   * Action to initialize process.
   *
   * @var string
   */
  const ACTION_INITIALIZE = 'initialize';

  /**
   * Action to pull the secret key from API.
   *
   * @var string
   */
  const ACTION_PULL_SECRET_KEY = 'pull_secret_key';

  /**
   * Process lifetime in seconds.
   *
   * @var int
   */
  const PROCESS_LIFETIME = 3600; /* sec. */

  /**
   * Default registration API domain.
   *
   * @var string
   */
  const DEFAULT_API_DOMAIN = 'https://developers.rublon.com';


  /**
   * URL path for API methods.
   *
   * @var string
   */
  const URL_PATH_ACTION = '/consumers_registration';


  // Common technology flags:
  const TECHNOLOGY_PHP_SDK = 'rublon-php-sdk';
  const TECHNOLOGY_OTHER = 'other';


  /**
   * Registration API domain.
   *
   * @var string
   */
  protected $apiDomain = NULL;


  /**
   * Constructor.
   */
  public function __construct() {
    $this->apiDomain = self::DEFAULT_API_DOMAIN;
  }


  // ---------------------------------------------------------------------------
  // The only public method.
  /**
   * Action to perform when the communication URL has been invoked.
   *
   * @param string $action
   *   Action name.
   *
   * @final
   */
  final public function action($action) {
    switch ($action) {
      case 'initialize':
        $this->initialize();
        break;

      case 'update_system_token':
        // Save the system token given for the module.
        $this->updateSystemToken();
        break;

      case 'final_success':
        // Final success.
        $this->finalSuccess();
        break;

      case 'final_error':
        // Final error.
        $this->finalError();
        break;

    }
  }


  // ---------------------------------------------------------------------------
  // Major protected methods to override:
  /**
   * Final method when process was successful.
   *
   * To return to page and show an message please override this method in
   * a subclass. Don't forget to call the parent method first - it will
   * clean some garbage.
   *
   * @override
   */
  protected function finalSuccess() {
    if ($this->isUserAuthorized()) {
      $this->saveInitialParameters(NULL, NULL);
    }
  }


  /**
   * Final method when process was failed.
   *
   * To return to page and show an message please override this method in
   * a subclass. Don't forget to call the parent method first - it will
   * clean some garbage.
   *
   * @param string $msg
   *   The msg parameter, must be type string.
   *
   * @override
   */
  protected function finalError($msg = NULL) {
    if ($this->isUserAuthorized()) {
      $this->saveSystemToken(NULL);
      $this->saveSecretKey(NULL);
      $this->saveInitialParameters(NULL, NULL);
    }
  }


  // ---------------------------------------------------------------------------
  // Main actions final protected methods:
  /**
   * Initialize the module's registration process by generating temporary key.
   *
   * @final
   */
  final protected function initialize() {
    if ($this->isUserAuthorized()) {
      if ($this->saveInitialParameters($this->privateGenerateRandomString(), time())) {
        $this->privateEcho($this->getRegistrationForm());
      }
      else {
        $this->finalError('ERROR_CODE: INITIAL_PARAMS_SAVE_FAILURE');
      }
    }
    else {
      $this->finalError('ERROR_CODE: USER_NOT_AUTHORIZED');
    }
  }


  /**
   * Update system token.
   *
   * Save received by POST system token to local storage
   * and call the secret key pulling method.
   * To protect this method, the user have to be authorized
   * in local system and this must be POST request.
   *
   * @final
   */
  final protected function updateSystemToken() {
    if ($this->privateValidateGeneral()) {
      if ($this->isUserAuthorized()) {
        if ($system_token = $this->privateGetSystemTokenFromBase64($this->privateGet('systemToken'))) {
          if ($this->saveSystemToken($system_token)) {
            $this->pullSecretKey($system_token);
          }
          else {
            $this->finalError('ERROR_CODE: SYSTEM_TOKEN_SAVE_FAILURE');
          }
        }
        else {
          $this->finalError('ERROR_CODE: NO_SYSTEM_TOKEN_RECEIVED');
        }
      }
      else {
        $this->finalError('ERROR_CODE: USER_NOT_AUTHORIZED');
      }
    }
    else {
      $this->finalError('ERROR_CODE: INVALID_PROCESS_SESSION');
    }
  }


  /**
   * Pull the secret key from API server.
   *
   * Perform REST request to get generated secret key and save it in local
   * storage. Communication is signed by the temporary key.
   *
   * @param string $system_token
   *   The systemToken parameter, must be type string.
   *
   * @final
   */
  final protected function pullSecretKey($system_token) {
    if ($this->privateValidateGeneral()) {
      $response = $this->privatePullSecretKey($system_token);
      if (!empty($response['secretKey'])) {
        if ($this->saveSecretKey($response['secretKey'])) {
          if (!empty($response['profileId'])) {
            $this->handleProfileId($response['profileId']);
          }
          $this->finalSuccess();
        }
        else {
          $this->finalError('ERROR_CODE: SECRET_KEY_SAVE_FAILURE');
        }
      }
      else {
        $this->finalError('ERROR_CODE: NO_SECRET_KEY_RECEIVED');
      }
    }
    else {
      $this->finalError('ERROR_CODE: INVALID_PROCESS_SESSION');
    }
  }


  // ---------------------------------------------------------------------------
  // Minor protected methods - can be overriden:
  /**
   * Send string to the standard output.
   *
   * If your system have an ususual way to echo strings, override this
   * method in a subclass.
   *
   * @param string $str
   *   The str parameter, must be type string.
   */
  protected function privateEcho($str) {
    echo $str;
  }


  /**
   * Redirect to the given URL.
   *
   * If your system have an ususual way to perform HTTP redirects, override
   * this method in a subclass.
   *
   * @param string $url
   *   The url parameter, must be type string.
   */
  protected function privateRedirect($url) {
    $this->privateHeader('Location: ' . $url);
    $this->privateExit();
  }


  /**
   * Send HTTP header.
   *
   * If your system have an ususual way to send HTTP headers, override this
   * method in a subclass.
   *
   * @param string $str
   *   The str parameter, must be type string.
   */
  protected function privateHeader($str) {
    header($str);
  }

  /**
   * Interrupt the script execution.
   *
   * If you have to trigger some actions before exit, override this method
   * in a subclass.
   */
  protected function privateExit() {
    exit;
  }


  /**
   * Get POST parameter.
   *
   * If your system have an ususual way to get POST parameters, override
   * this method in a subclass.
   *
   * @param string $name
   *   The name parameter, must be type string.
   */
  protected function privatePost($name) {
    return (isset($_POST[$name]) ? $_POST[$name] : NULL);
  }

  /**
   * Get GET parameter.
   *
   * If your system have an ususual way to get GET parameters, override
   * this method in a subclass.
   *
   * @param string $name
   *   The name parameter, must be type string.
   */
  protected function privateGet($name) {
    return (isset($_GET[$name]) ? $_GET[$name] : NULL);
  }


  /**
   * Get the registration form.
   */
  protected function getRegistrationForm() {
    $action = $this->getAPIDomain() . self::URL_PATH_ACTION . '/' . self::ACTION_INITIALIZE;
    $result = '<form action="' . htmlspecialchars($action)
      . '" method="post" id="RublonConsumerRegistration">'
      . $this->privateGetHidden('projectUrl', $this->getProjectUrl())
      . $this->privateGetHidden('communicationUrl', $this->getCommunicationUrl())
      . $this->privateGetHidden('callbackUrl', $this->getCallbackUrl())
      . $this->privateGetHidden('tempKey', $this->getTempKey())
      . $this->privateGetHidden('projectData', json_encode($this->getProjectData()))
      . '<script>document.getElementById("RublonConsumerRegistration").submit();</script>
    <noscript><input type="submit" value="Register" /></noscript>
    </form>';
    return $result;
  }


  /**
   * Get hidden input field for POST form.
   *
   * @param string $name
   *   The name parameter, must be type string.
   * @param string $value
   *   The value parameter, must be type string.
   */
  protected function privateGetHidden($name, $value) {
    return '<input type="hidden" name="' . htmlspecialchars($name)
      . '" value="' . htmlspecialchars($value) . '" />';
  }


  /**
   * Get project's additional data.
   *
   * The data returned will be used upon consumer's registration
   * and are required. If any additional data is needed,
   * this method may be overwritten.
   */
  protected function getProjectData() {
    return array(
      'project-name' => $this->getProjectName(),
      'project-technology' => $this->getProjectTechnology(),
      'lib-version' => self::VERSION,
      'plugin-version' => RublonDrupal::VERSION,
    );
  }


  /**
   * Get registration API domain.
   */
  public function getAPIDomain() {
    return $this->apiDomain;
  }


  // ---------------------------------------------------------------------------
  // Core private methods:
  /**
   * Send a REST response to the standard output.
   *
   * If secret key given wrap response into RublonSignatureWrapper.
   *
   * @param mixed $response
   *   The response parameter, may be mixed type.
   * @param string $secret_key
   *   (optional) Secret key.
   */
  protected function privateRESTResponse($response, $secret_key = NULL) {
    $this->privateHeader('content-type: application/json; charset=UTF-8');
    if ($secret_key) {
      $response = RublonSignatureWrapper::wrap($secret_key, $response);
    }
    $response = (is_string($response) ? $response : json_encode($response));
    $this->privateEcho($response);
    $this->privateExit();
  }


  /**
   * Create an error response array.
   *
   * @param string $msg
   *   The msg parameter, must be type string.
   */
  protected function privateRESTError($msg) {
    return array('status' => 'ERROR', 'msg' => $msg);
  }


  /**
   * Parse signed message.
   *
   * @param mixed $response
   *   The response parameter, may be mixed type.
   * @param string $secret_key
   *   The secretKey parameter, must be type string.
   *
   * @throws Exception
   *    Standard exception thrown on various errors.
   */
  protected function privateParseMessage($response, $secret_key) {
    $response = json_decode($response, TRUE);
    if (!empty($response)) {
      if (!empty($response['data']) AND !empty($response['sign'])) {
        if (RublonSignatureWrapper::verifyData($response['data'], $secret_key, $response['sign'])) {
          $data = json_decode($response['data'], TRUE);
          if (!empty($data) AND isset($data['body'])) {
            $body = json_decode($data['body'], TRUE);
            if (is_array($body) AND !empty($body)) {
              return $body;
            }
            else {
              return $data['body'];
            }
          }
          else {
            throw new Exception('Invalid response data');
          }
        }
        else {
          throw new Exception('Invalid signature');
        }
      }
      elseif (!empty($response['status'])) {
        if ($response['status'] == 'ERROR') {
          throw new Exception(isset($response['msg']) ? $response['status'] : 'Error response');
        }
        else {
          return $response;
        }
      }
      else {
        throw new Exception('Invalid response');
      }
    }
    else {
      throw new Exception('Empty response');
    }
  }


  /**
   * Perform a HTTP request to pull the secret key.
   *
   * @param string $system_token
   *   The systemToken parameter, must be type string.
   */
  protected function privatePullSecretKey($system_token) {

    // Prepare the request parameters:
    $url = $this->getAPIDomain() . self::URL_PATH_ACTION . '/' . self::ACTION_PULL_SECRET_KEY;
    $params = array(
      'systemToken' => $system_token,
      'lib-version' => self::VERSION,
    );

    // Perform the request:
    try {
      $response = $this->privateRequest($url, $params);
    }
    catch (RublonException $e) {
      $this->finalError($this->privateGetErrorName($e->getCode()));
    }

    // Validate response:
    try {
      $response = $this->privateParseMessage($response, $this->getTempKey());
    }
    catch (Exception $e) {
      $this->finalError('ERROR_CODE: INVALID_RESPONSE');
    }

    return $response;

  }


  /**
   * Perform a HTTP request.
   *
   * @param string $url
   *   The url parameter, must be type string.
   * @param array $params
   *   The params parameter, must be type array.
   */
  protected function privateRequest($url, array $params = array()) {
    $consumer = new RublonConsumer(NULL, $this->getTempKey());
    $service = new RublonService2Factor($consumer);
    $request = new RublonRequest($service);
    return $request
      ->setRequestParams($url, $params)
      ->getRawResponse();
  }


  /**
   * Generate random string.
   *
   * @param int $len
   *   (optional) Random string length.
   */
  protected function privateGenerateRandomString($len = 100) {
    $chars = '1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
    $max = strlen($chars) - 1;
    $result = '';
    for ($i = 0; $i < $len; $i++) {
      $result .= $chars[mt_rand(0, $max)];
    }
    return $result;
  }


  /**
   * Validate general parameters of the registration process.
   *
   * Check temporary key and start time.
   */
  protected function privateValidateGeneral() {
    $temp_key = $this->getTempKey();
    $time = $this->getStartTime();
    return (
      !empty($temp_key)
      AND !empty($time)
      AND preg_match('/[a-z0-9]{100}/i', $temp_key)
      AND is_int($time)
      AND abs(time() - $time) <= self::PROCESS_LIFETIME
    );
  }

  /**
   * Get System Token from base64 parameter.
   *
   * @param string $data
   *   Base64 decoded data.
   */
  protected function privateGetSystemTokenFromBase64($data) {
    $obj = $this->privateParseMessage(base64_decode(urldecode($data)), $this->getTempKey());
    return $obj['systemToken'];
  }


  /**
   * Get error name by the exception code.
   *
   * @param int $code
   *   The code parameter, must be type int.
   */
  protected function privateGetErrorName($code) {
    $error_code = 'ERROR_CODE: RUBLON_EXCEPTION_';
    switch ($code) {
      case RublonException::CODE_CURL_NOT_AVAILABLE:
        $error_code .= 'CODE_CURL_NOT_AVAILABLE';
        break;

      case RublonException::CODE_INVALID_RESPONSE:
        $error_code .= 'CODE_INVALID_RESPONSE';
        break;

      case RublonException::CODE_RESPONSE_ERROR:
        $error_code .= 'CODE_RESPONSE_ERROR';
        break;

      case RublonException::CODE_CURL_ERROR:
        $error_code .= 'CODE_CURL_ERROR';
        break;

      case RublonException::CODE_CONNECTION_ERROR:
        $error_code .= 'CODE_CONNECTION_ERROR';
        break;

      default:
        $error_code .= 'OTHER_REQUEST_ERROR';
    }
    return $error_code;
  }


  // ---------------------------------------------------------------------------
  // Abstract methods necessary to implement:
  /**
   * Check if the user is authorized to perform Rublon module registration.
   *
   * Check whether user authenticated in current session
   * can perform administrative operations
   * such as registering the Rublon module.
   *
   * @abstract
   */
  abstract protected function isUserAuthorized();


  /**
   * Get current system token from local storage.
   *
   * Returns local-stored system token or NULL if empty.
   * Note that parameter must be available for different browsers
   * and IPs so cannot be stored in browser sesssion,
   * but in database or configuration file.
   *
   * @abstract
   */
  abstract protected function getSystemToken();


  /**
   * Save system token to the local storage.
   *
   * Save given system token into local storage.
   * Note that parameter must be available for different browsers
   * and IPs so cannot be stored in browser sesssion,
   * but in database or configuration file.
   *
   * Returns TRUE/FALSE on success/failure.
   *
   * @param string $system_token
   *   The systemToken parameter, must be type string.
   *
   * @abstract
   */
  abstract protected function saveSystemToken($system_token);


  /**
   * Get current secret key from local storage.
   *
   * Return local-stored secret key or NULL if empty.
   * Note that parameter must be available for different browsers
   * and IPs so cannot be stored in browser sesssion,
   * but in database or configuration file.
   *
   * @abstract
   */
  abstract protected function getSecretKey();

  /**
   * Save secret key to the local storage.
   *
   * Save given secret key into local storage.
   * Note that parameter must be available for different browsers
   * and IPs so cannot be stored in browser sesssion,
   * but in database or configuration file.
   *
   * Returns TRUE/FALSE on success/failure.
   *
   * @param string $secret_key
   *   The secretKey parameter, must be type string.
   *
   * @abstract
   */
  abstract protected function saveSecretKey($secret_key);

  /**
   * Handle profileId of the user registering the consumer.
   *
   * If the user's profileId was received along with the secretKey,
   * handle it accordingly
   * (e.g. secure the registering user's account if necessary).
   *
   * @param int $profile_id
   *   The profileId parameter, must be type int.
   *
   * @abstract
   */
  abstract protected function handleProfileId($profile_id);

  /**
   * Get temporary key from local storage.
   *
   * Returns local-stored temporary key or NULL if empty.
   * Temporary key is used to sign communication with API
   * instead of secret key which is not given.
   * Note that parameter must be available for different browsers
   * and IPs so cannot be stored in browser sesssion,
   * but in database or configuration file.
   */
  abstract protected function getTempKey();

  /**
   * Save temporary key to the local storage.
   *
   * Save given temporary key into local storage.
   * Temporary key is used to sign communication with API
   * instead of secret key which is not given.
   * Note that parameter must be available for different browsers
   * and IPs so cannot be stored in browser sesssion,
   * but in database or configuration file.
   *
   * Returns TRUE/FALSE on success/failure.
   *
   * @param string $temp_key
   *   The tempKey parameter, must be type string.
   *
   * @abstract
   */
  abstract protected function saveTempKey($temp_key);


  /**
   * Save initial parameters to the local storage.
   *
   * Save given temporary key and process start time into
   * local storage.
   * Note that parameters must be available for different browsers
   * and IPs so cannot be stored in browser sesssion,
   * but in database or configuration file.
   *
   * Returns TRUE/FALSE on success/failure.
   *
   * @param string $temp_key
   *   The tempKey parameter, must be type string.
   * @param int $start_time
   *   The startTime parameter, must be type int.
   *
   * @abstract
   */
  abstract protected function saveInitialParameters($temp_key, $start_time);


  /**
   * Get process start time from local storage.
   *
   * Return local-stored start time of the process or NULL if empty.
   * Start time is used to validate lifetime of the process.
   * Note that parameter must be available for different browsers
   * and IPs so cannot be stored in browser sesssion,
   * but in database or configuration file.
   *
   * @abstract
   */
  abstract protected function getStartTime();


  /**
   * Get the communication URL of this Rublon module.
   *
   * Returns public URL address of the communication script.
   * API server calls the communication URL to communicate
   * with local system by REST or browser redirections.
   * The communication URL is supplied to the API during initialization.
   *
   * @abstract
   */
  abstract protected function getCommunicationUrl();


  /**
   * Get project's public webroot URL address.
   *
   * Returns the main project URL address needful
   * for registration consumer in API.
   *
   * @abstract
   */
  abstract protected function getProjectUrl();

  /**
   * Get the callback URL of this Rublon module.
   *
   * Returns public URL address of the Rublon consumer's callback script.
   * API server calls the callback URL after valid authentication.
   * The callback URL is needful for registration consumer in API.
   *
   * @abstract
   */
  abstract protected function getCallbackUrl();


  /**
   * Get name of the project.
   *
   * Returns name of the project that will be set in Rublon Developers
   * Dashboard.
   */
  abstract protected function getProjectName();


  /**
   * Get project's technology.
   *
   * Returns technology, module or library name to set in project.
   */
  abstract protected function getProjectTechnology();


}
