<?php

/**
 * @file
 * Rublon.drupal.issuenotifier class file.
 */

/**
 * Subclass of the RublonIssueNotifier to implement Drupal-specific methods.
 *
 * @see RublonIssueNotifier
 */
class RublonDrupalIssueNotifier extends RublonIssueNotifier {


  /**
   * Rublon helper instance.
   *
   * @var RublonDrupal
   */
  protected $helper;


  /**
   * Constructor - set the Rublon helper instance.
   *
   * @param RublonDrupal $helper
   *   The helper parameter, must be type RublonDrupal.
   */
  public function __construct(RublonDrupal $helper) {
    $this->helper = $helper;
  }


  /**
   * Add Drupal specific variables into issue information.
   *
   * @see RublonIssueNotifier::formatData()
   */
  protected function formatData($issue, $options) {

    $data = parent::formatData($issue, $options);

    if (function_exists('ini_get_all')) {
      @ $data['context']['php']['ini'] = ini_get_all(NULL, FALSE);
    }
    if (function_exists('get_loaded_extensions')) {
      @ $data['context']['php']['loaded_extensions'] = get_loaded_extensions();
    }

    if (empty($data['profile_id'])) {
      $data['profile_id'] = RublonDrupal::getRublonProfileId();
    }

    $data['context']['_SERVER'] = $_SERVER;
    $data['context']['_POST'] = FALSE;
    $data['context']['_GET'] = FALSE;
    $data['context']['_COOKIE'] = FALSE;

    return $data;

  }

  /**
   * Get API domain.
   *
   * @see RublonIssueNotifier::getDomain()
   */
  protected function getDomain() {
    return RublonDrupal::getAPIDomain();
  }

  /**
   * Returns module's technology tag.
   *
   * @see RublonIssueNotifier::getTechnology()
   */
  protected function getTechnology() {
    return RublonDrupal::getTechnology();
  }

  /**
   * Send notification by browser output handler.
   *
   * @see RublonIssueNotifier::sendByBrowser()
   */
  protected function sendByBrowser(array $options) {
    $content = RublonText::getMessage('IN_NOTIFICATION_SENT');
    $content .= $this->getBrowserIssueForm($options);
    drupal_set_message($content, 'error');
    return TRUE;
  }

  /**
   * Get current URL address.
   *
   * @see RublonIssueNotifier::getCurrentUrl()
   */
  protected function getCurrentUrl() {
    return url(NULL, array('absolute' => TRUE));
  }

}
