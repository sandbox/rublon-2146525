<?php

/**
 * @file
 * Rublon.callback class file.
 */

/**
 * Implementation of the Rublon2FactorCallbackTemplate.
 *
 * @see Rublon2FactorCallbackTemplate
 */
class RublonCallback extends Rublon2FactorCallbackTemplate {

  /**
   * Error during authentication.
   *
   * @var int
   */
  const ERROR_WHEN_AUTH = 100;

  /**
   * User's ID to authorize.
   *
   * @var int
   */
  protected $userIdToAuthorize = NULL;


  /**
   * Get state from GET parameters or NULL if not present.
   *
   * @see Rublon2FactorCallbackTemplate::getState()
   */
  protected function getState() {
    return $this->inputGet('state');
  }

  /**
   * Get access token from GET parameters or NULL if not present.
   *
   * @see Rublon2FactorCallbackTemplate::getAccessToken()
   */
  protected function getAccessToken() {
    return $this->inputGet('token');
  }

  /**
   * Handle an error and return back to the previous page.
   *
   * @see Rublon2FactorCallbackTemplate::finalError()
   */
  protected function finalError($error_code, $details = NULL) {

    switch ($error_code) {
      case self::ERROR_REST_CREDENTIALS:
        $msg = RublonText::getMessage('RC_FAILED_TO_AUTHENTICATE_USER');
        break;

      case self::ERROR_UNKNOWN_ACTION_FLAG:
      case self::ERROR_MISSING_ACTION_FLAG:
        $msg = RublonText::getMessage('RC_UNKNOWN_ACTION_TO_PERFORM');
        break;

      case self::ERROR_USER_NOT_AUTHORIZED:
        $msg = RublonText::getMessage('RC_NOT_AUTHENTICATED_FIRST_FACTOR');
        break;

      case self::ERROR_DIFFERENT_USER:
        $msg = RublonText::getMessage('RC_EXPECTED_DIFFERENT_USER');
        break;

      case self::ERROR_WHEN_AUTH:
        $msg = RublonText::getMessage('RC_DRUPAL_AUTH_PROBLEM');
        break;

      case 'PLUGIN_OUTDATED':
        $msg = RublonText::getMessage('RC_PLUGIN_OUTDATED');
        break;

      default:
        $msg = RublonText::getMessage('RC_ERROR');
    }

    RublonDrupal::error($msg, array(
      'method' => __METHOD__,
      'file' => __FILE__,
      'error' => $error_code,
      'uid' => $this->userIdToAuthorize,
      'details' => $details,
    ));

    $this->goBack();

  }

  /**
   * Get technology code name.
   *
   * @see Rublon2FactorCallbackTemplate::getTechnology()
   */
  protected function getTechnology() {
    return RublonDrupal::getTechnology();
  }


  /**
   * Restore session with passed session ID.
   *
   * @see Rublon2FactorCallbackTemplate::sessionStart()
   */
  protected function sessionStart($session_id) {
    if (!empty($session_id)) {
      if ($session_name = $this->getConsumerParam('sessionName')) {

        $params = session_get_cookie_params();
        $expire = $params['lifetime'] ? REQUEST_TIME + $params['lifetime'] : 0;
        setcookie($session_name, $session_id, $expire, $params['path'], $params['domain'], $params['secure'], $params['httponly']);

      }
    }
  }

  /**
   * Check standard authorization status.
   *
   * Check whether user exists in session and has been authorized by first
   * factor.
   *
   * @see Rublon2FactorCallbackTemplate::isUserAuthorizedByFirstFactor()
   */
  protected function isUserAuthorizedByFirstFactor() {
    if ($uid = $this->getConsumerParam('uid')) {
      $this->userIdToAuthorize = $uid;
      if ($this->isLoginAction()) {
        return TRUE;
      }
      else {
        return RublonDrupal::isUserAuthenticated($uid);
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get Rublon profile ID of the user in current session.
   *
   * @see Rublon2FactorCallbackTemplate::getRublonProfileId()
   */
  protected function getRublonProfileId() {
    if ($this->isLoginAction()) {
      if ($uid = $this->getConsumerParam('uid')) {
        $this->userIdToAuthorize = $uid;
        return RublonDrupal::getRublonProfileId($uid);
      }
      else {
        return 0;
      }
    }
    else {
      return RublonDrupal::getRublonProfileId();
    }
  }

  /**
   * Set Rublon profile ID of the user in current session.
   *
   * @see Rublon2FactorCallbackTemplate::setRublonProfileId()
   */
  protected function setRublonProfileId($rublon_profile_id) {
    RublonDrupal::setRublonProfileId($rublon_profile_id);
  }

  /**
   * Set second-factor authorization status.
   *
   * Set second-factor authorization status of the user in current session to
   * SUCCESS.
   *
   * @see Rublon2FactorCallbackTemplate::authorizeUser()
   */
  protected function authorizeUser() {
    if ($uid = $this->getConsumerParam('uid')) {
      $this->userIdToAuthorize = $uid;
      RublonDrupal::loginUser($uid);
    }
    else {
      $this->finalError(self::ERROR_WHEN_AUTH);
    }
  }

  /**
   * Cancel authentication process and return back to the previous page.
   *
   * @see Rublon2FactorCallbackTemplate::cancel()
   */
  protected function cancel() {
    $this->goBack();
  }

  /**
   * Handle success.
   *
   * @see Rublon2FactorCallbackTemplate::finalSuccess()
   */
  protected function finalSuccess() {

    // Flash message:
    switch ($this->getConsumerParam('actionFlag')) {
      case RublonAuthParams::ACTION_FLAG_LINK_ACCOUNTS:
        drupal_set_message(RublonText::getMessage('RC_RUBLON_ENABLED'));
        break;

      case RublonAuthParams::ACTION_FLAG_UNLINK_ACCOUNTS:
        drupal_set_message(RublonText::getMessage('RC_RUBLON_DISABLED'));
        break;

    }

    $this->goBack();

  }

  /**
   * Retrieve consumer's systemToken.
   *
   * @see Rublon2FactorCallbackTemplate::getSystemToken()
   */
  protected function getSystemToken() {
    return RublonDrupal::getSystemToken();
  }

  /**
   * Retrieve consumer's secretKey.
   *
   * @see Rublon2FactorCallbackTemplate::getSecretKey()
   */
  protected function getSecretKey() {
    return RublonDrupal::getSecretKey();
  }

  /**
   * Get current language code.
   *
   * @see Rublon2FactorCallbackTemplate::getLang()
   */
  protected function getLang() {
    return RublonDrupal::getLang();
  }

  /**
   * Return the (optional) Rublon API domain.
   *
   * @see Rublon2FactorCallbackTemplate::getAPIDomain()
   */
  protected function getAPIDomain() {
    return RublonDrupal::getAPIDomain();
  }


  /**
   * Helper method to obtain GET-passed parameter.
   *
   * @param string $name
   *   The name parameter, must be type string.
   */
  protected function inputGet($name) {
    $query = drupal_get_query_parameters();
    return (isset($query[$name]) ? $query[$name] : NULL);
  }


  /**
   * Returns consumer param with given name or NULL if not exists.
   *
   * @param string $name
   *   The name parameter, must be type string.
   */
  protected function getConsumerParam($name) {
    if (!empty($this->response)) {
      $consumer_params = $this->response->getConsumerParams();
      if (!empty($consumer_params) AND isset($consumer_params[$name])) {
        return $consumer_params[$name];
      }
      else {
        return NULL;
      }
    }
    else {
      return NULL;
    }
  }

  /**
   * Check whether current action flag equals "login".
   */
  protected function isLoginAction() {
    return ($this->getConsumerParam('actionFlag') == RublonAuthParams::ACTION_FLAG_LOGIN);
  }


  /**
   * Return to previous page.
   */
  public function goBack() {
    if (RublonDrupal::isUserAuthenticated()) {
      if ($destination = $this->getConsumerParam('customURIParam')
        OR $destination = $this->inputGet('custom')) {
        RublonDrupal::redirect($destination);
      }
      else {
        RublonDrupal::redirect(RublonDrupal::getUserEditURL());
      }
    }
    else {
      RublonDrupal::redirect(NULL);
    }
  }


}
